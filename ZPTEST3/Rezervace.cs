﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZPTEST3
{
    public class Rezervace
    {
        public int id { get; private set; }
        public int uzId { get; private set; }
        public int filmId { get; private set; }
        public DateTime date { get; private set; }

        public Rezervace(int id, int uzId, int filmId, DateTime date)
        {
            this.id = id;
            this.uzId = uzId;
            this.filmId = filmId;
            this.date = date;
        }
    }
}
