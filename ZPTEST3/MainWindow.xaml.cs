﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DB;
using MySqlConnector;
using System.Windows.Automation;

namespace ZPTEST3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;
        List<Film> listFilmu = new List<Film>();
        List<Rezervace> listRezervaci = new List<Rezervace>();
        List<Uzivatel> listUzivatelu = new List<Uzivatel>();
        public MainWindow()
        {
            this.dbconn = new DB_connect();

            LogInWin loginwin = new LogInWin();
            loginwin.ShowDialog();

            InitializeComponent();
            filmy();
            rezervace();
            zakaznik();
        }

        public void filmy()
        {
            string sel = "SELECT * FROM Film";
            reader = this.dbconn.Select(sel);
            while (reader.Read())
            {
                listFilmu.Add(new Film(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3)));
                
            }
            this.dbconn.CloseConn();

            foreach(var o in this.listFilmu)
            {
                listboxFilm.Items.Add(String.Format("{0}", o.title));
            }
        }

        public void rezervace()
        {
            string sel = "SELECT * FROM Rezervace";
            reader = this.dbconn.Select(sel);
            while (reader.Read())
            {
                listRezervaci.Add(new Rezervace(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2), reader.GetDateTime(3)));
            }
            this.dbconn.CloseConn();

            foreach (var o in this.listRezervaci)
            {
                listboxRezerv.Items.Add(String.Format("{0}", o.date));
            }
        }

        public void zakaznik()
        {
            string sel = "SELECT * FROM Uzivatel";
            reader = this.dbconn.Select(sel);
            while (reader.Read())
            {
                listUzivatelu.Add(new Uzivatel(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetInt32(5)));
            }
            this.dbconn.CloseConn();

            foreach (var o in this.listUzivatelu)
            {
                listboxZak.Items.Add(String.Format("{0} {1}", o.name, o.surname));
            }
        }

        private void btnVytZak_Click(object sender, RoutedEventArgs e)
        {
            ADDzak addzak = new ADDzak();
            addzak.ShowDialog();

            dbconn.Insert(String.Format("INSERT INTO Uzivatel(name, surname, nickname, password, role)" + "VALUES( '{0}', '{1}', '{2}', '{3}', '{4}')", addzak.txbName.Text, addzak.txbSurname.Text, addzak.txbNickname.Text, addzak.txbPassword.Text, addzak.txbRole.Text));
            MessageBox.Show("Uživatel byl přidán");
        }

        public Uzivatel getByName (string name)
        {
            foreach(var o in listUzivatelu)
            {
                if(o.name == name)
                {
                    return o;
                }
            }
            return null;
        }

        public Film getByFilm(string title)
        {
            foreach(var o in listFilmu)
            {
                if(o.title == title)
                {
                    return o;
                }
            }
            return null;
        }
        private void btnSmazZak_Click(object sender, RoutedEventArgs e)
        {
            string s = listboxZak.SelectedItem.ToString();
            Uzivatel uzi1 = getByName(s);
            if(uzi1 != null)
            {
                dbconn.Delete(String.Format("DELETE FROM Uzivatel where id={0}", uzi1.id));
            }
            listboxZak.Items.Remove(listboxZak.SelectedItem);
            MessageBox.Show("Zákazník byl smazán");
        }

        private void btnSmazRez_Click(object sender, RoutedEventArgs e)
        {
            string s = listboxFilm.SelectedItem.ToString();
            Film film1 = getByFilm(s);
            if (film1 != null)
            {
                dbconn.Delete(String.Format("SELECT * FROM Rezervace LEFT JOIN Film ON Rezervace.filmId=Film.id WHERE Film.id={0}", film1.id));
            }
            MessageBox.Show("Rezervace byla smazána");
        }
        public void nacti(string sel)
        {
            listUzivatelu.Clear();
            listboxZak.Items.Clear();

            reader = this.dbconn.Select(sel);
            while (reader.Read())
            {
                listUzivatelu.Add(new Uzivatel(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetInt32(5)));
            }
            this.dbconn.CloseConn();

            foreach (var o in this.listUzivatelu)
            {
                listboxZak.Items.Add(String.Format("{0} {1}", o.name, o.surname));
            }
        }

        private void btnSurname_Click(object sender, RoutedEventArgs e)
        {
            string sel = "SELECT * FROM Uzivatel ORDER BY surname";
            nacti(sel);
        }

        private void btnName_Click(object sender, RoutedEventArgs e)
        {
            string sel = "SELECT * FROM Uzivatel ORDER BY name";
            nacti(sel);
        }

        private void btnNickname_Click(object sender, RoutedEventArgs e)
        {
            string sel = "SELECT * FROM Uzivatel ORDER BY nickname";
            nacti(sel);
        }
    }
}
