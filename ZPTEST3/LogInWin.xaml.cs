﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DB;
using MySqlConnector;
using System.Windows.Automation;

namespace ZPTEST3
{
    /// <summary>
    /// Interaction logic for LogInWin.xaml
    /// </summary>
    public partial class LogInWin : Window
    {
        //testivaci data pro přihlášení přezdívka a heslo 
        //zakaznik annsmet annsmet
        //admin tomnovy tomnovy

        private DB_connect dbconn;
        private MySqlDataReader reader;
        List<Uzivatel> listUzivatelu = new List<Uzivatel>();
        public LogInWin()
        {
            this.dbconn = new DB_connect();
            InitializeComponent();
        }

        private void btnLogIn_Click(object sender, RoutedEventArgs e)
        {
            
            string sel = "SELECT * FROM Uzivatel";
            reader = this.dbconn.Select(sel);
            while (reader.Read())
            {
                listUzivatelu.Add(new Uzivatel(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetInt32(5)));
            }
            this.dbconn.CloseConn();

            foreach (var o in listUzivatelu)
            {
                if (o.nickname == txbNickname.Text.ToString() && o.password == txbPassWord.Text.ToString())
                {
                    if(o.role == 1)
                    {
                        Close();
                    }
                    else
                    {
                        UserWin userwin = new UserWin();
                        userwin.ShowDialog();
                    }                   
                }
                //vyskakuje i když jinak přihlášení funguje
                //else
                //{
                //    MessageBox.Show("Nesprávné přihlašovací jméno nebo heslo");
                //}
            }
        }
    }
}
