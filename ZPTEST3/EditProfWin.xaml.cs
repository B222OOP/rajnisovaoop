﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ZPTEST3
{
    /// <summary>
    /// Interaction logic for EditProfWin.xaml
    /// </summary>
    public partial class EditProfWin : Window
    {
        public EditProfWin()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Změny uloženy");
            Close();
        }
    }
}
