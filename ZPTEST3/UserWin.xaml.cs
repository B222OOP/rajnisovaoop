﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DB;
using MySqlConnector;
using System.Windows.Automation;

namespace ZPTEST3
{
    /// <summary>
    /// Interaction logic for UserWin.xaml
    /// </summary>
    public partial class UserWin : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;
        List<Film> listFilmu = new List<Film>();
        public UserWin()
        {
            this.dbconn = new DB_connect();
            InitializeComponent();
            film();
            
        }
        
        public void film()
        {
            string sel = "SELECT * FROM Film";
            reader = this.dbconn.Select(sel);
            while (reader.Read())
            {
                listFilmu.Add(new Film(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3)));

            }
            this.dbconn.CloseConn();

            foreach (var o in this.listFilmu)
            {
                listboxFilm.Items.Add(String.Format("{0}", o.title, o.resf)); //1=rezervovaný, 0=volný
            }
        }
      
        //private void btnRezerv_Click(object sender, RoutedEventArgs e)
        //{
           
        //}

        public Film getByTitle(string title)
        {
            foreach (var o in listFilmu)
            {
                if (o.title == title)
                {
                    return o;
                }
            }
            return null;
        }

        private void btnProfil_Click(object sender, RoutedEventArgs e)
        {
            EditProfWin editprofwin = new EditProfWin();
            editprofwin.ShowDialog();

            //dbconn.Update(String.Format("UPDATE Uzivatel SET Uzivatel.nickname={0}, Uzivatel.password={1} WHERE Uzivatel.id={2}", editprofwin.txbNickname.Text, editprofwin.txbPassword.Text));
        }

        public void nactiFilm(string sel)
        {
            listFilmu.Clear();
            listboxFilm.Items.Clear();

            reader = this.dbconn.Select(sel);
            while (reader.Read())
            {
                listFilmu.Add(new Film(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3)));

            }
            this.dbconn.CloseConn();
            foreach (var o in this.listFilmu)
            {
                listboxFilm.Items.Add(String.Format("{0} {1}", o.title, o.resf));
                
            }
        }

        private void btnNazev_Click(object sender, RoutedEventArgs e)
        {
            string sel = "SELECT * FROM Film ORDER BY title";
            nactiFilm(sel);            
            
        }

        private void btnZanr_Click(object sender, RoutedEventArgs e)
        {
            string sel = "SELECT * FROM Film ORDER BY genre";
            nactiFilm(sel);
        }
    }
}
