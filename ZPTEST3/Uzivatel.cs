﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZPTEST3
{
    public class Uzivatel
    {
        public int id { get; private set; } 
        public string name { get; private set; }
        public string surname { get; private set; }
        public string nickname { get; private set; }
        public string password { get; private set; }

        public int role { get; private set; }

        public Uzivatel(int id, string name, string surname, string nickname, string password, int role)
        {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.nickname = nickname;
            this.password = password;
            this.role = role;
        }   
    }
}
