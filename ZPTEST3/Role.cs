﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZPTEST3
{
    public class Role
    {
        public int id { get; private set; }
        public string roleName { get; private set; }

        public Role(int id, string roleName)
        {
            this.id = id;
            this.roleName = roleName;
        }   
    }
}
