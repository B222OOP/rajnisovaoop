﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZPTEST3
{
    public class Film
    {
        public int id { get; private set; }
        public string title { get; private set; }
        public string genre { get; private set; }
        public int resf { get; private set; }

        public Film(int id, string title, string genre, int res)
        {
            this.id = id;
            this.title = title;
            this.genre = genre;
            this.resf = res;
        }
    }
}
