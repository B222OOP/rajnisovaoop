﻿#pragma checksum "..\..\..\UserWin.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "52D524D7E8C480BED7BDF1CCF478D94AAB755272"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using ZPTEST3;


namespace ZPTEST3 {
    
    
    /// <summary>
    /// UserWin
    /// </summary>
    public partial class UserWin : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\..\UserWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblFilm;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\..\UserWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox listboxFilm;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\UserWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnRezerv;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\UserWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnProfil;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\UserWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblFiltr;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\UserWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNazev;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\UserWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnZanr;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "6.0.10.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ZPTEST3;component/userwin.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UserWin.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "6.0.10.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.lblFilm = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.listboxFilm = ((System.Windows.Controls.ListBox)(target));
            return;
            case 3:
            this.btnRezerv = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.btnProfil = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\..\UserWin.xaml"
            this.btnProfil.Click += new System.Windows.RoutedEventHandler(this.btnProfil_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.lblFiltr = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.btnNazev = ((System.Windows.Controls.Button)(target));
            
            #line 15 "..\..\..\UserWin.xaml"
            this.btnNazev.Click += new System.Windows.RoutedEventHandler(this.btnNazev_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btnZanr = ((System.Windows.Controls.Button)(target));
            
            #line 16 "..\..\..\UserWin.xaml"
            this.btnZanr.Click += new System.Windows.RoutedEventHandler(this.btnZanr_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

