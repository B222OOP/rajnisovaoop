﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAC_MAN
{
    public class Player
    {
        public string Name { get; private set; }    
        public int Score { get; private set; }

        public Player (string Name, int Score)
        {
            this.Score = Score;
            this.Name = Name;
        }
       

        
        
    }
}
