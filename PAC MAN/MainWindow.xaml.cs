﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using DB;
using MySqlConnector;

namespace PAC_MAN
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer timer = new DispatcherTimer();

        bool goLeft, goRight, goDown, goUp;
        bool noLeft, noRight, noDown, noUp;

        int speed = 8;

        int ghostMoveStep = 120;
        int currGhostStep;
        int ghostSpeed = 10;

        int score = 0;

        Rect HitBox;

        public DB_connect dbconn;
        public MySqlDataReader reader;
        List<Player> players = new List<Player>();


        public MainWindow()
        {
                      
            this.dbconn = new DB_connect();

            InitializeComponent();

            
            PlayerWin playerwin = new PlayerWin();
            playerwin.ShowDialog();

            lbl2.Content = playerwin.txb.Text;

            GameSetUp();
        }
        private void Cnv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left && noLeft == false)
            {
                goLeft = true;
                goRight = goUp = goDown = false;
                noRight = noUp = noDown = false;

            }

            if (e.Key == Key.Right && noRight == false)
            {
                goRight = true;
                goLeft = goUp = goDown = false;
                noLeft = noUp = noDown = false;
            }

            if (e.Key == Key.Up && noUp == false)
            {
                goUp = true;
                goLeft = goRight = goDown = false;
                noLeft = noRight = noDown = false;
            }

            if (e.Key == Key.Down && noDown == false)
            {
                goDown = true;
                goLeft = goRight = goUp = false;
                noLeft = noRight = noUp = false;
            }
        }
        public void GameSetUp()
        {
            Cnv.Focus();

            timer.Tick += GameLoop;
            timer.Interval = TimeSpan.FromMilliseconds(20);
            timer.Start();
            currGhostStep = ghostMoveStep;

        }

        public void GameOver(string message)
        {
            timer.Stop();
            MessageBox.Show(message, " ");

            System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
            Application.Current.Shutdown();
        }

        public void GameLoop(object sender, EventArgs e)
        {
            lbl1.Content = "Score: " + score;

            //pohyb pacmana
            if (goRight)
            {
                Canvas.SetLeft(Pac_man, Canvas.GetLeft(Pac_man) + speed);
            }
            if (goLeft)
            {
                Canvas.SetLeft(Pac_man, Canvas.GetLeft(Pac_man) - speed);
            }
            if (goUp)
            {
                Canvas.SetTop(Pac_man, Canvas.GetTop(Pac_man) - speed);
            }
            if (goDown)
            {
                Canvas.SetTop(Pac_man, Canvas.GetTop(Pac_man) + speed);
            }

            //když narazí na konec obrazovky

            if (goDown && Canvas.GetTop(Pac_man) + 75 > Application.Current.MainWindow.Height)
            {
                noDown = true;
                goDown = false;
            }
            if (goUp && Canvas.GetTop(Pac_man) - 10 < 1)
            {
                noUp = true;
                goUp = false;
            }
            if (goLeft && Canvas.GetLeft(Pac_man) - 10 < 1)
            {
                noLeft = true;
                goLeft = false;
            }
            if (goRight && Canvas.GetLeft(Pac_man) + 55 > Application.Current.MainWindow.Width)
            {
                noRight = true;
                goRight = false;
            }

            HitBox = new Rect(Canvas.GetLeft(Pac_man), Canvas.GetTop(Pac_man), Pac_man.Width, Pac_man.Height);

            foreach (var o in Cnv.Children.OfType<Rectangle>())
            {
                Rect hitbox = new Rect(Canvas.GetLeft(o), Canvas.GetTop(o), o.Width, o.Height);

                if ((string)o.Tag == "wall")
                {
                    if (goLeft == true && HitBox.IntersectsWith(hitbox))
                    {
                        Canvas.SetLeft(Pac_man, Canvas.GetLeft(Pac_man) + 10);
                        noLeft = true;
                        goLeft = false;
                    }
                    if (goRight == true && HitBox.IntersectsWith(hitbox))
                    {
                        Canvas.SetLeft(Pac_man, Canvas.GetLeft(Pac_man) - 10);
                        noRight = true;
                        goRight = false;
                    }
                    if (goDown == true && HitBox.IntersectsWith(hitbox))
                    {
                        Canvas.SetTop(Pac_man, Canvas.GetTop(Pac_man) - 10);
                        noDown = true;
                        goDown = false;
                    }
                    if (goUp == true && HitBox.IntersectsWith(hitbox))
                    {
                        Canvas.SetTop(Pac_man, Canvas.GetTop(Pac_man) + 10);
                        noUp = true;
                        goUp = false;
                    }
                }

                if ((string)o.Tag == "coin")
                {
                    if (HitBox.IntersectsWith(hitbox) && o.Visibility == Visibility.Visible)
                    {
                        o.Visibility = Visibility.Hidden;
                        score++;
                    }
                }

                if ((string)o.Tag == "ghost")
                {
                    if (HitBox.IntersectsWith(hitbox))
                    {
                        Db();
                        GameOver("Game over");

                    }

                    if (o.Name.ToString() == "Ghostb")
                    {
                        Canvas.SetLeft(o, Canvas.GetLeft(o) - ghostSpeed);
                    }
                    else
                    {
                        Canvas.SetLeft(o, Canvas.GetLeft(o) + ghostSpeed);
                    }

                    currGhostStep--;

                    if (currGhostStep < 1)
                    {
                        currGhostStep = ghostMoveStep;
                        ghostSpeed = -ghostSpeed;
                    }
                }
            }

            if (score == 48)
            {
                Db();
                GameOver("Winner");

            }

        }
        public void Db()
        {
            this.reader = dbconn.Select("SELECT * FROM Players");
            while (reader.Read())
            {
                this.players.Add(new Player(reader.GetString(1), reader.GetInt32(2)));
            }
            this.dbconn.Insert(String.Format("INSERT INTO Players (Name, Score)" + "VALUES ('{0}', '{1}')", lbl2.Content.ToString(), score));

        }
    }
}
