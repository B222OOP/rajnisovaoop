﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Text;

namespace DB
{
    public class DB_connect
    {
        private string Mysqlconn;
        public MySqlConnection connection;
        public DB_connect()
        {
            Mysqlconn = "Server=kbi-sp.fbmi.cvut.cz;Database=rajnikat;Uid=rajnikat;Pwd=rajnikat1;";
        }
        public DB_connect(string server, string database, string uid, string pwd, bool datetime_convert)
        {
            Mysqlconn = String.Format("Server={0};Database={1};Uid={2};Pwd={3};Convert Zero Datetime={4};",server,database,uid,pwd,datetime_convert);
        }

        public MySqlDataReader Select(string command)
        {
            this.connection = new MySqlConnection(Mysqlconn);
            MySqlCommand cmd = new MySqlCommand(command, connection);
            connection.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            //CloseConn();
            return reader;
        }
        public void Insert(string command)
        {
            this.connection = new MySqlConnection(Mysqlconn);
            MySqlCommand cmd = new MySqlCommand(command, connection);
            connection.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            CloseConn();
        }
        public void CloseConn()
        {
            this.connection.Close();
        }

        public void Update(string command)
        {
            this.connection = new MySqlConnection(Mysqlconn);
            MySqlCommand cmd = new MySqlCommand(command, connection);
            connection.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            CloseConn();
        }

        public void Delete(string command)
        {
            this.connection = new MySqlConnection(Mysqlconn);
            MySqlCommand cmd = new MySqlCommand(command, connection);
            connection.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            CloseConn();
        }
    }
}
