﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace ZPTEST1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Patient> listpacientu = new List<Patient>();
            Patient pac1 = new Patient("Jan", "Novak", "808080/0214", 154, 180);
            listpacientu.Add(pac1);
            listpacientu.Add(new Patient("Ondrej", "Slany", "808080/0214", 154, 180));
            listpacientu.Add(new Patient("Josef", "Beruska", "808080/0214", 154, 180));
            listpacientu.Add(new Patient("Adam", "Novotny", "808080/0214", 154, 180));
            listpacientu.Add(new Patient("Lubos", "Novak", "808080/0214", 154, 180));
            List<Room> listpokoju = new List<Room>();
            listpokoju.Add(new Room(3, 180));
            listpokoju.Add(new Room(4, 150));
            

            Presun presun = new Presun (listpacientu);
            presun.pridejPacienta();
            pac1.vypis();

            Console.ReadKey();

        }
    }

    public class Patient
    {
        public string name { get; private set; }
        public string surname { get; private set; }
        public string rc { get; private set; }  
        public float hmotnost { get; set; }
        public float vyska { get; set; }

        public Patient (string name, string surname, string rc, float hmotnost, float vyska)
        {
            this.name = name;
            this.surname = surname;
            this.rc = rc;
            this.hmotnost = hmotnost;
            this.vyska = vyska;
        }

        public float BMI(float hmotnost, float vyska)
        {
            vyska = this.vyska/100;
            float bmi = (this.hmotnost) /(vyska*vyska);
            return bmi;
        }
        public string vypis()
        {
            float hmot = BMI(this.hmotnost, this.vyska);
            string str = String.Format("Pacient {0} {1}, rodné číslo: {2}, má hmotnost: {3}, výšku: {4} a BMI {5}", this.name, this.surname, this.rc, this.hmotnost, this.vyska, hmot);
            return str;

        }

    }
    public class Bed
        {
        public List<Patient> listpacientu { get; private set; }
        public string ozn { get; private set; }
        public float maxnos { get; private set; }
        public bool obsazene { get; set; }

        public Bed(string ozn, float maxnos, bool obsazene, List<Patient> listpacientu)
        {
            this.ozn = ozn;
            this.maxnos = maxnos;
            this.obsazene = obsazene;
            this.listpacientu = listpacientu;
        }
        
  }
        public class Room
    {
        public int mnluz { get; private set; }
        public float maxhm { get; private set; }

        public Room (int mnluz, float maxhm)
        {
            this.mnluz = mnluz;
            this.maxhm = maxhm;
        }

    }

    public class Presun
    {
        List<Patient> listik = new List<Patient>();
        
        public Patient pac1 { get; set; }   
        public Bed pos1 { get; set; }   
        public Room pok1 { get; set; }
        public List<Patient> listpacientu { get; private set; }
        public Presun (List<Patient> listpacientu, Patient pac1, Bed pos1, Room pok1)
        {
            this.listpacientu = listpacientu;
            this.pac1 = pac1;
            this.pos1 = pos1;
            this.pok1 = pok1;
        }

        
        public void pridejPacienta()
        {
            if (listpacientu == null)
            {
                listpacientu = this.listpacientu;
            }
            if (this.pac1.hmotnost <= this.pos1.maxnos)
            {
                this.pos1.obsazene = true;
                
            }
            else
                Console.WriteLine("Pacient je příliš těžký, zvolte jiné lůžko");
            
        }


     



    }
}

