﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Drawing;
using System.Data.OleDb;
using static System.Net.Mime.MediaTypeNames;
using Path = System.IO.Path;
using System.Security.Cryptography.X509Certificates;

namespace TextEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            
            combobox.Items.Add("8");
            combobox.Items.Add("9");
            combobox.Items.Add("10");
            combobox.Items.Add("11");
            combobox.Items.Add("12");
            combobox.Items.Add("14");

            listbox1.Items.Add("Arial");
            listbox1.Items.Add("Cambria");
            listbox1.Items.Add("Times New Roman");
           

            //fontsize();
            //fontstyle();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            
            string path = @"C:\Users\kater\OneDrive\Plocha\ŠKOLA\2.ročník\OOP\TextEditor\Text\";
            string filename = "Text.txt";
            string text = txb.Text;
            using (StreamWriter sw = new StreamWriter(Path.Combine(path, filename)))
            {
                sw.WriteLine(text);
            }
        }

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            string path = @"C:\Users\kater\OneDrive\Plocha\ŠKOLA\2.ročník\OOP\TextEditor\Text\";
            string filename = "Text.txt";
            using (StreamReader sr = new StreamReader(Path.Combine(path, filename)))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    txb.Text = line;
                }
            }
        }

        private void btnBold_Click(object sender, RoutedEventArgs e)
        {
            if (txb.FontWeight == FontWeights.Bold)
            {
                txb.FontWeight = FontWeights.Thin;
            }
            else
            {
                txb.FontWeight = FontWeights.Bold;
            }
        }

        private void btnItalic_Click(object sender, RoutedEventArgs e)
        {
            if (txb.FontStyle == FontStyles.Italic)
            {
                txb.FontStyle = FontStyles.Normal;
            }
            else
            {              
                txb.FontStyle = FontStyles.Italic;
            }
        }

        private void btn8_Click(object sender, RoutedEventArgs e)
        {
            txb.FontSize = 8;
        }

        private void btn10_Click(object sender, RoutedEventArgs e)
        {
            txb.FontSize = 10;
        }

        private void btn12_Click(object sender, RoutedEventArgs e)
        {
            txb.FontSize = 12;
        }

        private void btn14_Click(object sender, RoutedEventArgs e)
        {
            txb.FontSize = 14;
        }

        private void btnArial_Click(object sender, RoutedEventArgs e)
        {
            txb.FontFamily = new FontFamily("Arial");
        }

        private void btnTNR_Click(object sender, RoutedEventArgs e)
        {
            txb.FontFamily = new FontFamily("Times New Roman");
        }

        //public void fontsize()
        //{
        //    string str = combobox.SelectedItem.ToString();
        //    txb.FontSize = Int32.Parse(str);
        //}

        //public void fontstyle()
        //{

        //    txb.FontFamily = new FontFamily(((ListBoxItem)listbox1.SelectedItem).Content.ToString());
        //}


    }
}
