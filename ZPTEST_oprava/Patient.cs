﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZPTEST_oprava
{
    internal class Patient
    {
        public string name { get; private set; }
        public string surname { get; private set; }
        public string rc { get; private set; }
        public float hmotnost { get; set; }
        public float vyska { get; set; }

        public Patient(string name, string surname, string rc, float hmotnost, float vyska)
        {
            this.name = name;
            this.surname = surname;
            this.rc = rc;
            this.hmotnost = hmotnost;
            this.vyska = vyska;
        }

        public float BMI(float hmotnost, float vyska)
        {
            vyska = this.vyska / 100;
            float bmi = (this.hmotnost) / (vyska * vyska);
            return bmi;
        }
        public void vypis()
        {
            float hmot = BMI(this.hmotnost, this.vyska);
            string str = String.Format("Pacient: {0} {1}, rodné číslo: {2}, má hmotnost: {3}, výšku: {4} a BMI {5}\n", this.name, this.surname, this.rc, this.hmotnost, this.vyska, hmot);
            Console.WriteLine(str);

        }

        public float hubnuti(float hmotnost)
        {
            Console.WriteLine("Zadejte novou váhu pacienta:");
            float novavaha = float.Parse(Console.ReadLine());
            float zhb = hmotnost - novavaha;
            hmotnost = novavaha;
            Console.WriteLine("pacient zhubl o: {0} kg", zhb);
            return hmotnost;

        }

        public float nabrani(float hmotnost)
        {
            Console.WriteLine("Zadejte novou váhu pacienta:");
            float novavaha1 = float.Parse(Console.ReadLine());
            float nbr = novavaha1 - hmotnost;
            hmotnost = novavaha1;
            Console.WriteLine("Pacientova váha se zvýšila o: {0} kg", nbr);
            return hmotnost;
        }

    }
}
