﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZPTEST_oprava
{
    internal class Pridej
    {
        public List<Patient> listpacientu { get;  set; }
        public List<Room> listpokoju { get; set; }
        public List<Bed> listposteli { get; set; }
                

        public Pridej(List<Patient> listpacientu, List<Room> listpokoju, List<Bed> listposteli)
        {
            this.listpacientu = listpacientu;
            this.listpokoju = listpokoju;
            this.listposteli = listposteli;                 

        }      
                           
        public string luzko(List<Bed>listposteli)
        {
            foreach(Bed b in listposteli)
            {
                if(b.obsazene == false)
                {
                    string str = b.ozn;                
                    return str;
                }
            }
            return "";
        }

              

        
        public void pridaniPacienta()
        {
            
            for (int i = 0; i < listpacientu.Count; i++)
            {
                if (i >= listposteli.Count)
                {
                    Console.WriteLine("Pro pacienta {0} {1} není na klinice místo", this.listpacientu[i].name, this.listpacientu[i].surname);
                }
                else
                {
                    if (listpacientu[i].hmotnost <= listpokoju[0].maxhm)
                    {

                        if (listpokoju[0].mnluz > 0)
                        {

                            listposteli[i].obsadpostel();
                            string str = luzko(listposteli);
                            Console.WriteLine("Pacient: {0} {1} byl přidán na lůžko {2}", this.listpacientu[i].name, this.listpacientu[i].surname, str);
                            listpokoju[0].minusluz();                                                  
                            listpacientu[i].vypis();

                        }
                        else
                        {
                            if (listpokoju[1].mnluz > 0)
                            {

                                listposteli[i].obsadpostel();
                                string str = luzko(listposteli);
                                Console.WriteLine("Pacient {0} {1} byl přidán na lůžko {2}", this.listpacientu[i].name, this.listpacientu[i].surname, str);
                                listpokoju[1].minusluz();
                                listpacientu[i].vypis();
                            }
                            else
                            {
                                Console.WriteLine("Pro pacienta {0} {1} není na klinice místo", this.listpacientu[i].name, this.listpacientu[i].surname);
                                
                                listpacientu[i].vypis();
                            }

                        }

                    }
                    else
                    {
                        if (listpacientu[i].hmotnost <= listpokoju[1].maxhm)
                        {
                            if (listpokoju[1].mnluz > 0)
                            {

                                listposteli[i].obsadpostel();
                                string str = luzko(listposteli);
                                Console.WriteLine("Pacient {0} {1} byl přidán na lůžko {2}", this.listpacientu[i].name, this.listpacientu[i].surname, this.listposteli[i].ozn);
                                listpokoju[1].minusluz();
                                listpacientu[i].vypis();
                            }
                            else
                            {
                                Console.WriteLine("Pro pacienta {0} {1} není na klinice místo", this.listpacientu[i].name, this.listpacientu[i].surname);
                                
                                listpacientu[i].vypis();
                            }

                        }
                        else
                        {
                            Console.WriteLine("Pro pacienta {0} {1} není na klinice lůžko s dostatečnou nosností", this.listpacientu[i].name, this.listpacientu[i].surname);
                            
                            listpacientu[i].vypis();
                        }

                    }
                }

            }
        }
    }
}
