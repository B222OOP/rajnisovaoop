﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZPTEST_oprava
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Patient> listpacientu = new List<Patient>();
            listpacientu.Add(new Patient("Jan", "Novak", "808080/0214", 185, 180));
            listpacientu.Add(new Patient("Ondrej", "Slany", "808080/0214", 220, 180));
            listpacientu.Add(new Patient("Josef", "Beruska", "808080/0214", 154, 180));
            listpacientu.Add(new Patient("Adam", "Novotny", "808080/0214", 154, 180));
            listpacientu.Add(new Patient("Lubos", "Novak", "808080/0214", 200, 180));
     

            List<Room> listpokoju = new List<Room>();
            listpokoju.Add(new Room(3, 180));       
            listpokoju.Add(new Room(2, 200));

            List<Bed> listposteli = new List<Bed>();
            listposteli.Add(new Bed("L1",180, true));
            listposteli.Add(new Bed("L2", 180, true));
            listposteli.Add(new Bed("L3", 180, true));
            listposteli.Add(new Bed("L4", 200, true));
            listposteli.Add(new Bed("L5", 200, true));


           
            Pridej pridejpac = new Pridej(listpacientu, listpokoju, listposteli);
            pridejpac.pridaniPacienta();

            Console.ReadKey();
        }
       
    }
}
