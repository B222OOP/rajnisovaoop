﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZPTEST2
{
    class Bed
    {
        public string ozn;
        public int maxnos;
        public bool obsazeno;

        public Bed(string ozn, int maxnos, bool obsazeno)
        {
            this.ozn = ozn;
            this.maxnos = maxnos;
            this.obsazeno = obsazeno;
        }   
    }
}
