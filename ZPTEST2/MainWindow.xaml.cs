﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZPTEST2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Patient> listpacientu = new List<Patient>();
        List<Room> listpokoju = new List<Room>();
        List<Bed> listposteli = new List<Bed>();
        

        
        public MainWindow()
        {
            InitializeComponent();
        }
        private void btnloadpat_Click(object sender, RoutedEventArgs e)
        {
            foreach (var o in listpacientu) { listboxPat.Items.Add(o.name); }
        }


        private void btnCreatePat_Click(object sender, RoutedEventArgs e)
        {
            PatientWin patientwin = new PatientWin();
            patientwin.ShowDialog();
            listpacientu.Add(new Patient(patientwin.txbpatname.Text, patientwin.txbpatsurname.Text, Int32.Parse(patientwin.txbpatrc.Text),Int32.Parse(patientwin.txbpatvaha.Text), Int32.Parse(patientwin.txbpativyska.Text)));
            pridejPac(listpacientu[0]);
        }


        private void btnCreateRoom_Click(object sender, RoutedEventArgs e)
        {
            RoomWin roomwin = new RoomWin();
            roomwin.ShowDialog();
            listpokoju.Add(new Room(roomwin.txbozn.Text,Int32.Parse(roomwin.txbmnluz.Text), Int32.Parse(roomwin.txbnosnluz.Text)));
            int a = Int32.Parse(roomwin.txbmnluz.Text);
            int x = 1;
            for (int i = 0; i < a ; i++)
            {            
                listposteli.Add(new Bed("P"+x, Int32.Parse(roomwin.txbnosnluz.Text), false));
                x++;
            }
            reloadListBox();
        }

        private void btnnahrajpokoj_Click(object sender, RoutedEventArgs e)
        {
            foreach(var o in listpokoju) { listboxPokoje.Items.Add(o.name); }
        }
  

        private void btnzobrazpostele_Click(object sender, RoutedEventArgs e)
        {
            
            if (listboxPost.Visibility == Visibility.Hidden)
            {
                listboxPost.Visibility = Visibility.Visible;
            }
            
            listboxPost.Items.Clear();

            int j = 0;
            j = listboxPokoje.SelectedIndex;
            
            for (int i = 0; i < listpokoju[j].mnluz; i++)
            {         
                listboxPost.Items.Add(listposteli[i].ozn);                                                           
            }
           
           
        }

        private void reloadListBox()
        {
            foreach(var o in listpokoju) { if (!listboxPokoje.Items.Contains(o.name)) { listboxPokoje.Items.Add(o.name); } }
        }

        

        public void pridejPac(Patient pac1)
        {
            for (int i = 0; i < listposteli.Count; i++)
            {
                
                if (listposteli[i].obsazeno == false)
                {
                    if (pac1.vaha < listposteli[i].maxnos)
                    {
                        listposteli[i].obsazeno = true;
                        tblockpac.Text = "Pacient byl pridan na luzko";

                    }
                    else
                    {
                        tblockpac.Text = "Pacient nemohl byt pridan na luzko";
                        tblockpac.Background = new SolidColorBrush(Colors.Red);
                        return;
                    }
                }
                else
                {
                    tblockpac.Text = "Pacient nemohl byt pridan na luzko";
                    tblockpac.Background = new SolidColorBrush(Colors.Red);
                    return;
                }
            }
        }

        private void btnpackart_Click(object sender, RoutedEventArgs e)
        {
           
            listboxPat.Items.Remove(listboxPat.SelectedItem);
        }
    }
}
