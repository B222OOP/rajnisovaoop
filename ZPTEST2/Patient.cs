﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZPTEST2
{
    public class Patient
    {
        public string name;
        public string surname;
        public int rc;
        public int vaha;
        public int vyska;
       
        public Patient(string name, string surname, int rc, int vaha, int vyska)
        {
            this.name = name;
            this.surname = surname;
            this.rc = rc;
            this.vaha = vaha;
            this.vyska = vyska; 
        }
    }
}
